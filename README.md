
Steps to run this application:

1. Clone this bitbucket repository
2. Navigate to the folder `ApplicationInfo`
3. Build the project with `mvn clean package`
4. Run the application with `java -jar target/ApplicationInfo.jar`
5. Open the browser on `http://localhost:8090/version`
