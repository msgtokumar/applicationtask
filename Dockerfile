FROM java:8-jdk-alpine

COPY ./target/ApplicationInfo.jar /usr/app/

WORKDIR /usr/app

RUN sh -c 'touch ApplicationInfo.jar'

ENTRYPOINT ["java", "-jar", "ApplicationInfo.jar"]
