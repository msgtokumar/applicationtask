include k8sinfra/Dev.properties
OS := $(shell uname)
K8S_FILES := $(shell cat k8sinfra/templates.txt)
DOCKER_IMAGE_NAME := "${Test}"
MAKE_ENV += DOCKER_IMAGE_NAME APP_NAME APPNAME REPLICAS_COUNT CONTAINER_NAME CONTAINER_PORT SERVICE_TYPE BUILD_VER K8S_BUILD_DIR K8S_FILES SERVICE_PORT_TYPE SERVICE_PORT SERVICE_TARGETPORT SERVICE_PROTOCAL BUILD_ID DNSNAME
SHELL_EXPORT := $(foreach v,$(MAKE_ENV),$(v)='$($(v))' )
container_Image:
	@echo "build starts."
	docker login -u ${USERNAME} -p ${PASSWORD} docker.io"
	docker build -f Dockerfile -t  ${DOCKER_IMAGE_NAME}:${IMAGE_TAG} ."
	docker tag ${DOCKER_IMAGE_NAME}:${IMAGE_TAG}  ${DOCKER_IMAGE_NAME}:${IMAGE_TAG}"
	docker push ${DOCKER_IMAGE_NAME}:${IMAGE_TAG}"
	@echo "Build & Taging Completed.."
.PHONY: provision
deploy_Container_stack:
	@echo "test start"
	$(shell mkdir -p $(K8S_BUILD_DIR))
	@for file in $(K8S_FILES); do \
	   mkdir -p `dirname "$(K8S_BUILD_DIR)/$$file"` ; \
	   $(SHELL_EXPORT) envsubst <$(K8S_DIR)/$$file >$(K8S_BUILD_DIR)/$$file ;\
	done
	kubectl apply -f $(K8S_BUILD_DIR)/service.yml
	kubectl apply -f $(K8S_BUILD_DIR)/deployment.yml
	kubectl get svc |grep ${APPNAME}| awk '{print $$4}'
	@echo "provision Completed."